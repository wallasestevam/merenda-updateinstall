package UpdateInstall.Controle.Utilitarios;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import wallas.util.event.progressEvent.ProgressListener;
import wallas.util.fileCopyStream.FileCopyStream;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class InstallComponents {

    private final FileCopyStream fcs;

    public InstallComponents() {
        fcs = new FileCopyStream();
    }

    public void install(String pathSource, String pathDestiny) throws FileNotFoundException, IOException {
        fcs.copyStream(getClass().getResource(pathSource).openStream(), pathDestiny);
    }

    public void install(ArrayList<String[]> listInstall) throws IOException {
        for (String[] listInstalls : listInstall) {
            install(listInstalls[0], listInstalls[1]);
        }
    }

    public void addProgressListener(ProgressListener listener) {
        fcs.addProgressListener(listener);
    }

    public void removeProgressListener(ProgressListener listener) {
        fcs.removeProgressListener(listener);
    }

}
