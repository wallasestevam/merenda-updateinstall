package UpdateInstall.Controle.Utilitarios;

import java.io.IOException;
import java.util.ArrayList;
import wallas.application.path.PathApplication;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class InitInstall extends InstallComponents {

    public InitInstall() {
    }

    public void initInstall() throws IOException {
        ArrayList<String[]> arrayList = new ArrayList<>();

        arrayList.add(new String[]{"/UpdateInstall/Pacotes/DrawTest.jar", PathApplication.PATH_DIR_ROOT + "DrawTest.jar"});
        arrayList.add(new String[]{"/UpdateInstall/Pacotes/nv.conf", PathApplication.PATH_DIR_ROOT + "nv.conf"});

        install(arrayList);
    }

}
