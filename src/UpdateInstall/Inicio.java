package UpdateInstall;

import UpdateInstall.Visao.Utilitarios.JFrameUpdateInstall;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class Inicio {

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        JFrameUpdateInstall updateInstall = new JFrameUpdateInstall();
        updateInstall.setVisible(true);
    }

}
