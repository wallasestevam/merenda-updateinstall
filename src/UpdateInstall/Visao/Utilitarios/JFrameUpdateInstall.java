package UpdateInstall.Visao.Utilitarios;

import UpdateInstall.Controle.Utilitarios.InitInstall;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.WindowConstants;
import wallas.application.execute.ExecuteApplication;
import wallas.application.path.PathApplication;
import wallas.util.event.progressEvent.ProgressEvent;
import wallas.util.event.progressEvent.ProgressListener;

/**
 *
 * @author José Wallas Clemente Estevam
 */
public class JFrameUpdateInstall extends JDialog {

    private JLabel jLabelAtuEncontrada;
    private JLabel jLabelMensagens;
    private JLabel jLabelErroMessage;
    private JLabel jLabelStatus;
    private JPanel jPanelText;
    private JProgressBar jProgressBar;
    private InitInstall controleInstall;

    public JFrameUpdateInstall() {
        initComponents();
    }

    private void initComponents() {
        jProgressBar = new JProgressBar();
        jLabelAtuEncontrada = new JLabel();
        jLabelStatus = new JLabel();
        jLabelMensagens = new JLabel();
        jLabelErroMessage = new JLabel();
        jPanelText = new JPanel();
        controleInstall = new InitInstall();

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setResizable(false);

        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icones/padrao.png")));
        setTitle("Software Update");

        jLabelAtuEncontrada.setFont(new Font("Tahoma", 1, 11));
        jLabelAtuEncontrada.setText("Aguarde enquanto o sistema instala as atualizações...");

        jLabelStatus.setFont(new Font("Tahoma", 1, 11));
        jLabelStatus.setText("Status:");

        jProgressBar.setStringPainted(true);
        jProgressBar.setVisible(false);

        controleInstall.addProgressListener(componentsActionPerformed());

        layoutCadastro();

        controlUpdate().start();
    }

    private void layoutCadastro() {
        javax.swing.GroupLayout jPanelTextLayout = new javax.swing.GroupLayout(jPanelText);
        jPanelText.setLayout(jPanelTextLayout);
        jPanelTextLayout.setHorizontalGroup(
                jPanelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelTextLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanelTextLayout.createSequentialGroup()
                                        .addComponent(jLabelAtuEncontrada)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                .addGroup(jPanelTextLayout.createSequentialGroup()
                                        .addComponent(jLabelStatus)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                                        .addComponent(jLabelMensagens, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelTextLayout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jLabelErroMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
        );
        jPanelTextLayout.setVerticalGroup(
                jPanelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanelTextLayout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(jLabelAtuEncontrada)
                        .addGap(18, 18, 18)
                        .addGroup(jPanelTextLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabelStatus)
                                .addComponent(jLabelMensagens, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabelErroMessage, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanelText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jProgressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanelText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jProgressBar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(30, Short.MAX_VALUE))
        );

        setSize(new java.awt.Dimension(496, 188));
        setLocationRelativeTo(null);
    }

    private Thread controlUpdate() {
        return new Thread() {
            @Override
            public void run() {
                try {
                    jLabelMensagens.setText("Instalado Atualização...");
                    jProgressBar.setVisible(true);

                    controleInstall.initInstall();
                    jLabelMensagens.setText("Instalação Concluída...");

                    fileLogInstall();

                    dispose();
                    ExecuteApplication.initJFrameApplication();

                } catch (IOException ex) {
                    jLabelMensagens.setText("O sistema não conseguiu instalar atualizacão, se persistir contate o fornecedor do software!");
                    jLabelErroMessage.setText(ex.getMessage());

                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        JOptionPane.showMessageDialog(null, e.getMessage(), "Mensagem", JOptionPane.ERROR_MESSAGE);
                    }

                    dispose();
                    ExecuteApplication.initJFrameApplication();
                }
            }
        };
    }

    private void fileLogInstall() throws IOException {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");

        String string = PathApplication.PATH_DIR_UPDATE + sdf.format(c.getTime()) + "_" + PathApplication.NEXT_VERSION + "_install.txt";

        FileWriter arquivo = new FileWriter(new File(string));
        arquivo.write(sdf.format(c.getTime()));
        arquivo.close();
    }

    private ProgressListener componentsActionPerformed() {
        return new ProgressListener() {

            @Override
            public void progressStatus(ProgressEvent evt) {
                jProgressBar.setValue(evt.getProgress());
            }

            @Override
            public void nameProgressFile(ProgressEvent evt) {
                jLabelMensagens.setText(evt.getNameItem());
            }
        };
    }

}
